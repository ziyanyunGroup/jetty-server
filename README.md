# jetty-server

## 项目介绍
jetty-server 主要用于项目开发阶段，可实现项目的快速启动与热加载，从而加快项目的开发速度

## Maven 坐标
```
<dependency>
	<groupId>com.jfinal</groupId>
	<artifactId>jetty-server</artifactId>
	<version>2019.3</version>
</dependency>
```

## Jetty 嵌入式启动
需要使用 jetty 嵌入式启动用于生产环境，或者需要自定义启动方式可以参考 jetty 官方实例：
http://www.eclipse.org/jetty/documentation/current/embedded-examples.html

上述文档中提供了各种实例代码，例如支持 SSL

## 支持 JSP 以及 JSTL
在使用 jetty-server 开发需要支持 JSP、JSTL 时，需要添加如下依赖：
```
<dependency>
	<groupId>org.eclipse.jetty</groupId>
	<artifactId>jetty-jsp</artifactId>
	<version>9.2.26.v20180806</version>
	<scope>provided</scope>
</dependency>
```
在 IDEA 下开发时需要将上述配置的 scope 改成 compile，但如果打包部署在独立的容器中时需要再改成 provided